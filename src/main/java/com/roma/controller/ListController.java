package com.roma.controller;

import com.roma.entities.TodoList;
//import com.roma.repository.TodoListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin
@RequestMapping("api/list")
public class ListController {

//    @Autowired
//    TodoListRepository todoListRepository;

//    @GetMapping
//    public List<TodoList> helloWorld()
//    {
//        return (List<TodoList>) todoListRepository.findAll();
//    }

    @GetMapping
    public String helloWorld()
    {
        return "(List<TodoList>) todoListRepository.findAll()";
    }

    @GetMapping("{id}")
    public String getList(@PathVariable String id) {
        return "List" + id;
    }
}
