package com.roma.entities;

//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//@Entity
//@Table(name="todoitem")
public class TodoItem {
    //@Id
    private int id;

    private boolean isDone;

    private String text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "TodoItem{" +
                "id=" + id +
                ", isDone=" + isDone +
                ", text='" + text + '\'' +
                '}';
    }
}
