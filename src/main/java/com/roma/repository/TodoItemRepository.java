package com.roma.repository;

import com.roma.entities.TodoItem;
import org.springframework.data.repository.CrudRepository;

public interface TodoItemRepository extends CrudRepository<TodoItem, Long>
{

}